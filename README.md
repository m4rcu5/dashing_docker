# Containerized Dashing dashboard
With this Docker image you can easily add a pretty Dashing dashboard to any project.  

## Usage
Launch a default [Dashing](http://dashing.io "Dashing's Homepage") demo container: `docker run -d -p 8080:3030 m4rcu5/dashing:v1`    

It may be slightly more useful to use this image in a docker-compose project though. All you need is local directory cointaining a pre-generated Dashing project. With docker-compose you can then use your own directory as a volume inside this container and overwrite the default `WORKINGDIR` to point to your volume. Your docker-compose file would look something like this:
```YAML
version: '2'
services:
  dashing:
    image: m4rcu5/dashing:v1
    ports: 
      - "0.0.0.0:8080:3030"
    volumes:
      - ./custom-dashing:/custom-dashing:rw
    working_dir: /custom-dashing
    entrypoint: /usr/local/bin/dashing start
```