FROM ubuntu:xenial

RUN apt-get update && \
	apt-get -y install \
	build-essential \
	ruby \
	ruby-dev \
	nodejs

RUN gem install dashing
RUN gem install bundler

RUN mkdir /dashing &&\
	dashing new dashing &&\
	cd dashing &&\
	bundler

EXPOSE 3030

WORKDIR /dashing

ENTRYPOINT /usr/local/bin/dashing start 
